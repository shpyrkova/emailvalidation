module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',
    ["jest-html-reporter", {
      "publicPath": "./html-report",
      "filename": "report.html",
      "expand": true
    }],
    'jest-stare',
    ["jest-nyan-reporter", {
      "suppressErrorReporter": false
    }],  
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
module.exports = {
  testEnvironment: 'node',
  reporters: [
    'default',
    ["jest-html-reporters", {
      "publicPath": "./html-report",
      "filename": "report.html",
      "expand": true
    }],
  ],
  moduleFileExtensions: ['js', 'json'],
  transform: {
    '^.+\\.jsx?$': 'babel-jest',
  },
  testMatch: ['**/specs/*.spec.*'],
  globals: {
    testTimeout: 50000,
  },
  verbose: true,
};
