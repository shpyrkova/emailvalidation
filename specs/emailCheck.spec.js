import { apiProvider } from '../framework/services';
import * as context from '../framework/context/emailCheck.context';

test('В ответе присутствуют параметры отправленного email', async () => {
  const email = context.Email();
  const { body, status } = await apiProvider().emailcheck().get(context.accessKey, email);
  expect(status).toBe(200);
  expect(body.email).toBe(email.toLowerCase());
  expect(body.user).toBe(email.split('@')[0].toLowerCase());
  expect(body.domain).toBe(email.split('@')[1].toLowerCase());
});

test('Возвращается ошибка на запрос без accessKey', async () => {
  const email = context.Email();
  const { body, status } = await apiProvider().emailcheck().get('', email);
  expect(status).toBe(200);
  expect(body.success).toBe(false);
  expect(body.error.type).toBe('missing_access_key');
  expect(body.error.info).toBe('You have not supplied an API Access Key. [Required format: access_key=YOUR_ACCESS_KEY]');
});

describe('Для невалидных значений email параметр format_valid = false', () => {
  test.each`
  email        
  ${'test'}      
  ${'@123'}        
  ${'123@'}        
  ${'test@ru'} 
`('Невалидное значение email $email', async ({ email }) => {
    const { body, status } = await apiProvider().emailcheck().get(context.accessKey, email);
    expect(status).toBe(200);
    expect(body.email).toBe(email);
    expect(body.format_valid).toBe(false);
  });
});
