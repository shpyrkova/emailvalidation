import { EmailCheck } from './email/emailCheck.service';

const apiProvider = () => ({
  emailcheck: () => new EmailCheck(),
});

export { apiProvider };
